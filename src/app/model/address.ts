namespace App.Model {
  
    export interface Address {
        name: string;
        streetNumber: number;
        zipCode: number;
    }
}
