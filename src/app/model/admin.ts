namespace App.Model {

    export class Admin extends User {

        constructor(login: string, password: string) {
            super(login, password);
            this.role = Role.Admin;
        }
    }
}
