namespace App.Model {

    export class User {
        // Public attributs
        login: string;
        password: string;
        role: Role;
        address: Address;

        // Constructor
        constructor(login: string, password: string) {
            this.login = login;
            this.password = password;
            this.role = Role.Watcher;
        }

        // Methods
        checkPassword(password: string): boolean {
            return this.password === password;
        }
    }
}
