namespace App.Auth {

    angular.module("App").controller("App.AuthController", AuthController);

    class AuthController {

      static $inject = ["$scope"];

      constructor(private $scope: any) {
      }
    }
}
