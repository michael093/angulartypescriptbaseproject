namespace App {

    class AppRoute {

        static $inject = ["$routeProvider"];

        constructor($routeProvider: ng.route.IRouteProvider) {

            $routeProvider
                .when("/auth", {
                    templateUrl: "/auth/auth.html",
                    controller: "App.AuthController"
                })
                .when("/details", {
                    templateUrl: "/details/details.html",
                    controller: "App.DetailsController"
                });
        }
    }

    angular.module("App").config(AppRoute);
}
