/// <reference path="../typings/angularjs/angular.d.ts" />
/// <reference path="../typings/angularjs/angular-route.d.ts" />

namespace App {
    angular.module("App", ["ngRoute"]);
}
