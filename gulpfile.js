var gulp = require('gulp');

// To remove empty folders
var deleteEmpty = require('delete-empty');

// To clean project dist
var del = require('del');

// To uglify project
var uglify = require('gulp-uglify');

// To concat js files
var concat = require("gulp-concat");

// To compile typescript files
var ts = require("gulp-typescript");
var tsProject = ts.createProject("tsconfig.json");

// List bower files
var mainBowerFiles = require('main-bower-files');

// Constants
var dir = {
    source: './src',
    temp: './temp',
    destination: './dist'
}

// Move files from src to dist
gulp.task('srcToDist', ['clean'], function() {
    return gulp.src([dir.source + "/**/*", "!" + dir.source + "/**/*.ts"])
        .pipe(gulp.dest(dir.destination));
});

// Move bower modules to dist/libs
gulp.task('copyBowerModules', ['clean'], function() {
    gulp.src(mainBowerFiles())
        .pipe(gulp.dest(dir.destination + "/app/libs"));
});

// Compile typescript
gulp.task('tsc', ['clean'], function() {
    return tsProject.src()
        .pipe(tsProject())
        .js.pipe(gulp.dest(dir.temp + '/app'));
})

// Clean build destination
gulp.task('clean', function() {
    return del([
        dir.destination + "/**/*.*",
        dir.destination + "/**",
        dir.temp + "/**/*.*",
        dir.temp + "/**"
    ]);
});

// Concatenate all *.js in one js file (For production)
gulp.task('concatJsProd', ['uglify'], function() {
    return gulp.src(dir.temp + "/**/*.js")
        .pipe(concat('ocp.js'))
        .pipe(gulp.dest(dir.destination + "/app"));
});

// Concatenate all *.js in one js file (For developpement)
gulp.task('concatJsDev', ['tsc'], function() {
    return gulp.src(dir.temp + "/**/*.js")
        .pipe(concat('ocp.js'))
        .pipe(gulp.dest(dir.destination + "/app"));
});

// Uglify all js
gulp.task('uglify', ['tsc'], function() {
    return gulp.src(dir.temp + "/**/*.js")
        .pipe(uglify())
        .pipe(gulp.dest(dir.temp));
});

// Remove every empty folder
gulp.task('deleteEmpty', ['copyBowerModules', 'srcToDist'], function() {
    deleteEmpty.sync(dir.destination);
});

// Default task
gulp.task('default', ['compile-dev']);

// Compile for developpement (Skip Uglify)
gulp.task('compile-dev', ['clean', 'tsc', 'concatJsDev', 'srcToDist', 'copyBowerModules', 'deleteEmpty'], function() {
    process.stdout.write("********* Developpement *********\n");
});

// Compile for production
gulp.task('compile-prod', ['clean', 'tsc', 'uglify', 'concatJsProd', 'srcToDist', 'copyBowerModules', 'deleteEmpty'], function() {
    process.stdout.write("********* Production *********\n");
});
